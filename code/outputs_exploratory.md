Outputs exploratory
========================================================


Pre-data
Head, columns type
-------------------------


```r
setwd("C:/Work/Kaggle/kdd/code/")
require(ellipse)
```

```
## Loading required package: ellipse
```

```r
require(ggplot2)
```

```
## Loading required package: ggplot2
```

```r

df <- read.csv("../data/outcomes.csv")

head(df)
```

```
##                          projectid is_exciting
## 1 ffffc4f85b60efc5b52347df489d0238           f
## 2 ffffac55ee02a49d1abc87ba6fc61135           f
## 3 ffff97ed93720407d70a2787475932b0           f
## 4 ffff418bb42fad24347527ad96100f81           f
## 5 ffff2d9c769c8fb5335e949c615425eb           t
## 6 fffeebf4827d745aa36b17c2d38d1966           f
##   at_least_1_teacher_referred_donor fully_funded at_least_1_green_donation
## 1                                              f                          
## 2                                 f            t                         t
## 3                                 f            t                         t
## 4                                 f            f                         t
## 5                                 t            t                         t
## 6                                 f            t                         f
##   great_chat three_or_more_non_teacher_referred_donors
## 1          f                                          
## 2          f                                         t
## 3          t                                         t
## 4          t                                         f
## 5          t                                         f
## 6          f                                         f
##   one_non_teacher_referred_donor_giving_100_plus
## 1                                               
## 2                                              f
## 3                                              t
## 4                                              f
## 5                                              t
## 6                                              t
##   donation_from_thoughtful_donor great_messages_proportion
## 1                                                       NA
## 2                              f                        57
## 3                              f                       100
## 4                              f                       100
## 5                              f                        63
## 6                              f                         0
##   teacher_referred_count non_teacher_referred_count
## 1                     NA                         NA
## 2                      0                          7
## 3                      0                          3
## 4                      0                          1
## 5                      6                          2
## 6                      0                          1
```

```r

work.df <- df[, !(names(df) %in% "projectid")]
rm(df)

work.df[work.df == ""] <- NA


sapply(work.df, class)
```

```
##                                    is_exciting 
##                                       "factor" 
##              at_least_1_teacher_referred_donor 
##                                       "factor" 
##                                   fully_funded 
##                                       "factor" 
##                      at_least_1_green_donation 
##                                       "factor" 
##                                     great_chat 
##                                       "factor" 
##      three_or_more_non_teacher_referred_donors 
##                                       "factor" 
## one_non_teacher_referred_donor_giving_100_plus 
##                                       "factor" 
##                 donation_from_thoughtful_donor 
##                                       "factor" 
##                      great_messages_proportion 
##                                      "numeric" 
##                         teacher_referred_count 
##                                      "numeric" 
##                     non_teacher_referred_count 
##                                      "numeric"
```

```r

```

Table size 


```r
dim(work.df)
```

```
## [1] 619326     11
```


% of NAs in each column


```r
print(apply(work.df, MARGIN = 2, FUN = function(x) {
    sum(is.na(x)) * 100/length(x)
}))
```

```
##                                    is_exciting 
##                                           0.00 
##              at_least_1_teacher_referred_donor 
##                                          15.24 
##                                   fully_funded 
##                                           0.00 
##                      at_least_1_green_donation 
##                                          15.24 
##                                     great_chat 
##                                           0.00 
##      three_or_more_non_teacher_referred_donors 
##                                          15.24 
## one_non_teacher_referred_donor_giving_100_plus 
##                                          15.24 
##                 donation_from_thoughtful_donor 
##                                          15.24 
##                      great_messages_proportion 
##                                          29.04 
##                         teacher_referred_count 
##                                          15.24 
##                     non_teacher_referred_count 
##                                          15.24
```


Summary
-------------------

```r
summary(work.df, na.action = na.pass)
```

```
##  is_exciting at_least_1_teacher_referred_donor fully_funded
##  f:582616        :     0                       f:188643    
##  t: 36710    f   :400268                       t:430683    
##              t   :124660                                   
##              NA's: 94398                                   
##                                                            
##                                                            
##                                                            
##  at_least_1_green_donation great_chat
##      :     0               f:432156  
##  f   :146235               t:187170  
##  t   :378693                         
##  NA's: 94398                         
##                                      
##                                      
##                                      
##  three_or_more_non_teacher_referred_donors
##      :     0                              
##  f   :252550                              
##  t   :272378                              
##  NA's: 94398                              
##                                           
##                                           
##                                           
##  one_non_teacher_referred_donor_giving_100_plus
##      :     0                                   
##  f   :147119                                   
##  t   :377809                                   
##  NA's: 94398                                   
##                                                
##                                                
##                                                
##  donation_from_thoughtful_donor great_messages_proportion
##      :     0                    Min.   :  0              
##  f   :517992                    1st Qu.: 25              
##  t   :  6936                    Median : 50              
##  NA's: 94398                    Mean   : 53              
##                                 3rd Qu.: 83              
##                                 Max.   :100              
##                                 NA's   :179839           
##  teacher_referred_count non_teacher_referred_count
##  Min.   :  0            Min.   :  0               
##  1st Qu.:  0            1st Qu.:  1               
##  Median :  0            Median :  3               
##  Mean   :  1            Mean   :  4               
##  3rd Qu.:  0            3rd Qu.:  5               
##  Max.   :151            Max.   :304               
##  NA's   :94398          NA's   :94398
```

```r

```


Categorical Inputs
===============================

Tables and Fisher Test for Categorical variables vs. output

```r
output <- "is_exciting"
categorical.inputs <- names(sapply(work.df, class)[sapply(work.df, class) == 
    "factor"])
categorical.inputs <- setdiff(categorical.inputs, output)

for (input in categorical.inputs) {
    work.df[, input] <- factor(work.df[, input])
    tab <- table(work.df[, output], work.df[, input])
    print(tab)
    print(fisher.test(tab))
    
}
```

```
##    
##          f      t
##   f 400268  87950
##   t      0  36710
## 
## 	Fisher's Exact Test for Count Data
## 
## data:  tab
## p-value < 2.2e-16
## alternative hypothesis: true odds ratio is not equal to 1
## 95 percent confidence interval:
##  8402  Inf
## sample estimates:
## odds ratio 
##        Inf 
## 
##    
##          f      t
##   f 188643 393973
##   t      0  36710
## 
## 	Fisher's Exact Test for Count Data
## 
## data:  tab
## p-value < 2.2e-16
## alternative hypothesis: true odds ratio is not equal to 1
## 95 percent confidence interval:
##  4201  Inf
## sample estimates:
## odds ratio 
##        Inf 
## 
##    
##          f      t
##   f 146235 341983
##   t      0  36710
## 
## 	Fisher's Exact Test for Count Data
## 
## data:  tab
## p-value < 2.2e-16
## alternative hypothesis: true odds ratio is not equal to 1
## 95 percent confidence interval:
##  4201  Inf
## sample estimates:
## odds ratio 
##        Inf 
## 
##    
##          f      t
##   f 432156 150460
##   t      0  36710
## 
## 	Fisher's Exact Test for Count Data
## 
## data:  tab
## p-value < 2.2e-16
## alternative hypothesis: true odds ratio is not equal to 1
## 95 percent confidence interval:
##  8402  Inf
## sample estimates:
## odds ratio 
##        Inf 
## 
##    
##          f      t
##   f 247500 240718
##   t   5050  31660
## 
## 	Fisher's Exact Test for Count Data
## 
## data:  tab
## p-value < 2.2e-16
## alternative hypothesis: true odds ratio is not equal to 1
## 95 percent confidence interval:
##  6.253 6.645
## sample estimates:
## odds ratio 
##      6.446 
## 
##    
##          f      t
##   f 138104 350114
##   t   9015  27695
## 
## 	Fisher's Exact Test for Count Data
## 
## data:  tab
## p-value < 2.2e-16
## alternative hypothesis: true odds ratio is not equal to 1
## 95 percent confidence interval:
##  1.182 1.242
## sample estimates:
## odds ratio 
##      1.212 
## 
##    
##          f      t
##   f 482339   5879
##   t  35653   1057
## 
## 	Fisher's Exact Test for Count Data
## 
## data:  tab
## p-value < 2.2e-16
## alternative hypothesis: true odds ratio is not equal to 1
## 95 percent confidence interval:
##  2.274 2.600
## sample estimates:
## odds ratio 
##      2.432
```


Numerical Inputs
===============================



```r
numericas.inputs <- names(sapply(work.df, class)[sapply(work.df, class) != "factor"])

```


Histogramms
-----------------------------------


```r
for (nm in numericas.inputs) {
    hist(work.df[, nm], main = nm, col = "blue", xlab = "", freq = F)
}
```

![plot of chunk Histogramms](figure/Histogramms1.png) ![plot of chunk Histogramms](figure/Histogramms2.png) ![plot of chunk Histogramms](figure/Histogramms3.png) 

```r

```


Correlations
------------------------------

```r
cor.matr <- cor(work.df[, numericas.inputs], use = "na.or.complete")
print(cor.matr)
```

```
##                            great_messages_proportion
## great_messages_proportion                   1.000000
## teacher_referred_count                      0.006403
## non_teacher_referred_count                  0.064787
##                            teacher_referred_count
## great_messages_proportion                0.006403
## teacher_referred_count                   1.000000
## non_teacher_referred_count               0.108984
##                            non_teacher_referred_count
## great_messages_proportion                     0.06479
## teacher_referred_count                        0.10898
## non_teacher_referred_count                    1.00000
```

```r
plotcorr(cor.matr)
```

![plot of chunk Correlations](figure/Correlations.png) 


Scatter Plots
----------------------------------

```r
plot.df <- work.df[, c(numericas.inputs, output)]
no.plot.names <- output
for (nm in names(plot.df)) {
    no.plot.names <- c(no.plot.names, nm)
    names.diff <- setdiff(names(plot.df), no.plot.names)
    if (length(names.diff) < 1) 
        break
    
    for (nm2 in names.diff) {
        g <- ggplot(data = plot.df, aes(x = plot.df[, nm], y = plot.df[, nm2], 
            color = plot.df[, output])) + geom_point(size = 3, alpha = 0.75)
        g <- g + xlab(nm) + ylab(nm2) + scale_colour_discrete(name = output)
        plot(g)
    }
}
```

```
## Warning: Removed 179871 rows containing missing values (geom_point).
```

![plot of chunk Scatter Plots](figure/Scatter Plots1.png) 

```
## Warning: Removed 179871 rows containing missing values (geom_point).
```

![plot of chunk Scatter Plots](figure/Scatter Plots2.png) 

```
## Warning: Removed 94398 rows containing missing values (geom_point).
```

![plot of chunk Scatter Plots](figure/Scatter Plots3.png) 

